import re
import asyncio
import random
from doclib import *

games = {
    r'you\s*?don\'t\s*?know\s*?jack': 'You Don\'t Know Jack (1 - 8 players) is a comedy trivia game with wacky questions, wacky answers, and an element of betrayal',
    r'drawful': 'Drawful (3 - 8 players) is a game which asks users to draw a wacky prompt, then has all players do a Fibbage while trying to guess the prompt based on the image. Don\'t know what Fibbage is? Just ask!',
    r'word\s*?spud': 'Word Spud (3 - 8 players) gives you a word and asks for, essentially, a suffix for it, which can be anything from an actual suffix to another word to an entire phrase. It then gives the next player your suffix, and so on.',
    r'lie\s*?swatter': 'Lie Swatter (1 - 100 players) gives you a wacky statement and asks you to determine whether it\'s true or false.',
    r'fibbage': 'Fibbage (3 - 8 players) is a game which asks wacky questions and has players tripping each other up by writing lies for the others to pick and searching for the truth themselves.',
    r'quip\s*?lash': 'Quiplash (3 - 8 players) pits each pair of players against each other by giving them a prompt and having everyone else vote on the funniest answer to said prompt.',
    r'bidiots': 'Bidiots (3 - 8 players) has players bid on their own art.',
    r'earwax': 'Earwax (3 - 8 players) gives players a prompt and asks for two wacky noises to answer it.',
    r'bomb\s*?corp': 'Bomb Corp (1 - 4 players) is a bad game and you should feel bad for wondering about it.',
    r'guesspionage': 'Guesspionage (2 - 4 players) asks players to predict the results of a yearly Reddit survey question by wacky question.',
    r'tee\s*?k\.?\s*?o\.?': 'Tee K.O. (3 - 8 players) has players create shirt designs by coordinating other players\' drawings and captions.',

}


def tumble(chatbot, msg, matches):
    chatbot.set_nick('tumbleweed')
    chatbot.send_msg(parent=msg, msgString='/me rolls by')
    chatbot.set_nick('jackBot')


def intersection(lst1, lst2):
    return list(set(lst1) & set(lst2))


def kill(chatbot, msg, matches):
    chatbot.send_msg(parent=msg, msgString="/me ceases to annoy")
    chatbot.log("it ded", error=True)
    chatbot.kill()


def greatScott(chatbot, msg, matches):
    chatbot.set_nick('DocBrown')
    chatbot.send_msg(parent=msg, msgString='Great Scott, %s' % (msg.data.sender.name))
    chatbot.set_nick('jackBot')


def xyzzers(chatbot, msg, matches):
    if re.search("@xyzzy", msg.data.content) == None:
        chatbot.send_msg("Ask @Xyzzy.")


def honkkonk(chatbot, msg, matches):
    chatbot.set_nick('An Untitled Goose')
    chatbot.send_msg(parent=msg, msgString='Glory to Hong Kong!')
    chatbot.set_nick('jackBot')


def kirby(chatbot, msg, matches):
    if random.choice(range(1, 20)) == 20:
        chatbot.set_nick('Small Pink Orb')
        chatbot.send_msg(parent=msg, msgString='/me whispers "Use Kirby instead, it\'s much nicer"')
        chatbot.set_nick('jackBot')


def alive(chatbot, msg, matches):
    chatbot.send_msg(parent=msg, msgString='/me IS ALIVE!')
    chatbot.set_nick('Thunder')
    chatbot.send_msg(parent=msg, msgString='/me crashes')
    chatbot.set_nick('jackBot')


def myThing(chatbot, msg, matches):
    chatbot.set_nick('DocBrown')
    chatbot.send_msg(parent=msg, msgString='Hey, that\'s my thing!')
    chatbot.set_nick('jackBot')


def copyto(chatbot, msg, matches):
    chatbot.copy_to(roomName=matches.group(1))
    chatbot.send_msg(parent=msg, msgString="copying...")
    chatbot.log(f"copied to {matches.group(1)}")


def pm(chatbot, msg, matches):
    chatbot.initiate_pm(id=msg.data.sender.id)


def honk(chatbot, msg, matches):
    geese = re.findall(r'(?i)(\d*?|two|three|four|five|six|seven|eight|nine|ten)(^|\s|\b)g(oo|ee)se($|\s|\b)', msg.data.content, flags=0)
    ordinals = ["", "nother", " Third", " Fourth", " Fifth", " Sixth", " Seventh", "n Eighth", " Ninth", " Tenth"]
    numberOfGeese = 0
    gooseLevel = {"one": 1, "two": 2, "three": 3, "four": 4, "five": 5, "six": 6, "seven": 7, "eight": 8, "nine": 9, "ten": 10}
    for goose in geese:
        numberOfGeese += 1
        if goose[0] != '' and goose[0] != "1":
            if re.match(pattern=r"(\d+)", string=goose[0]):
                numberOfGeese += int(goose[0])
            elif re.match(pattern=r"(two|three|four|five|six|seven|eight|nine|ten)", string=goose[0]):
                numberOfGeese += gooseLevel.get(goose[0], 1)
    for goose in range(0, numberOfGeese - 1):
        if goose < 9:
            chatbot.set_nick(f'A{ordinals[goose]} Goose')
            chatbot.send_msg(parent=msg, msgString='Glory to Hong Kong!')
            chatbot.set_nick('jackBot')
        elif numberOfGeese > 15:
            chatbot.set_nick('Another Throng of Geese')
            chatbot.send_msg(parent=msg, msgString='Glory to Hong Kong!')
            chatbot.set_nick('The Final Goose')
            chatbot.send_msg(parent=msg, msgString='Glory to Hong Kong!')
            chatbot.set_nick('jackBot')
            break
        elif numberOfGeese == 10:
            chatbot.set_nick('The Final Goose')
            chatbot.send_msg(parent=msg, msgString='Glory to Hong Kong!')
            chatbot.set_nick('jackBot')
            break
        else:
            chatbot.set_nick('The Final Few Geese')
            chatbot.send_msg(parent=msg, msgString='Glory to Hong Kong!')
            chatbot.set_nick('jackBot')
            break


def linker(chatbot, msg, matches):
    if msg.data.sender != 'RedditLinker':
        users = re.findall(r'(?<![a-zA-Z0-9])/?u/([a-zA-Z0-9_-]{3,20})\b', msg.data.content, flags=0)
        for i in users:
            chatbot.set_nick('RedditLinker')
            chatbot.send_msg(parent=msg, msgString='reddit.com/u/%s' % i)
            chatbot.set_nick('jackBot')
        reddits = re.findall(r'(?<![a-zA-Z0-9])/?r/([a-zA-Z0-9_-]{2,21})\b', msg.data.content, flags=0)
        for i in reddits:
            chatbot.set_nick('RedditLinker')
            chatbot.send_msg(parent=msg, msgString='reddit.com/r/%s' % i)
            chatbot.set_nick('jackBot')


def room(chatbot, msg, matches):
    if msg.data.sender.name != 'Heimdall':
        message = 'You\'re in &%s! Welcome! Say hi, guys!' % (chatbot.room)
        chatbot.send_msg(message)


def userlist(chatbot, msg, matches):
    response = ""
    for user in chatbot.get_userlist():
        if "bot" not in user.id:
            response += "name: " + user.name + " id: " + user.id + " server_id: " + user.server_id + " server_era: " + user.server_era + " session_id: " + user.session_id + "\n"
    return response


def loggedin(chatbot, msg, matches):
    if chatbot.account != None:
        name = None
        for user in chatbot.get_userlist():
            if "bot" not in user.id:
                chatbot.log(user.id.lstrip(":"))
                if user.id.split(':')[1] == chatbot.account:
                    name = user.name
                    break
        if name != None:
            print(chatbot.send_msg(f'Logged in as {name}', parent=msg).data.sender)
        else:
            return f'Logged in as {chatbot.account}, who is not in this room.'
    else:
        return 'Not logged in.'


jackBot = Bot(nick="jackBot", room="xkcd", owner="DoctorNumberFour", help='I\'m a miscellaneous bot made by @DoctorNumberFour using his Python 3 bot library, DocLib (!doclib for a gitlab link.) I\'m slowly being updated to give info on every jackbox game.')
jackBot.set_regexes({
                    r'(?i)([\s\S]*?)where\.?\s?am\.?\s?i': room,
                    r'(?i)this\.?\s?town\.?\s?(ain\'t|aint|isn\'t|isnt|is not)\.?\s?big\.?\s?enough\.?\s?for\.?\s?(the\.?\s?two\.?\s?of\.?\s?us|the both of us|us two|both of us)': tumble,
                    r'(?i)gigawatt': greatScott,
                    r'(?i)back\.?\s?to\.?\s?the\.?\s?future': greatScott,
                    r'(?i)great\.?\s?scott': myThing, r'(?<![a-zA-Z0-9]{1}/)u/([a-zA-Z0-9_-]{3,20})\b': linker, r'(?<![a-zA-Z0-9]{1}/)r/([a-zA-Z0-9_-]{2,21})\b': linker,
                    r'^!kill @jackbot$': kill,
                    r'(?i)([\s\S]*?)where\.?\s?is(\.?\s?bot\.?\s?bot|([\s\S]*?)other\.?\s?bots)': 'BotBot is back up in &bots, courtesy of @DoctorNumberFour and @myhandsaretypingwords! If you want to use an external tool though, there\'s still yaboli (a Python library from Garmy), basebot (a Python library from Xyzzy), and DocLib (a Python library from DoctorNumberFour, used to make jackBot).\n use ![library name] for a link to the github/gitlab pages of each library to get started, or go to &bots to use the almighty BotBot (reborn as DotBot) again! (!tell @DNF if it\'s down lol)',
                    r'(?i)Point for user jackBot registered\.': 'Why thank you!',
                    r'(?i)^!games (dnf|doctornumberfour|dn4|@DoctorNumberFour)$': '@DoctorNumberFour has all of the Jackbox games.',
                    r'(?i)^!games$': 'dnf has all of them. ask him.',
                    r'^!players$': 'Most games require at least 3 players (though Guesspionage and Fibbage only require 2 and several only require 1, and Weapons Drawn requires 4) and can hold up to 8 players (though Bracketeering can hold up to 16, Bidiots and Zeeple Dome hold 6, and Bomb Corp can only hold 4).',
                    r'(?i)who([\s\S]*?)jack\.?\s?bot': 'I was made by @DoctorNumberFour using his own bot library, DocLib: https://gitlab.com/DoctorNumberFour/DocLib !',
                    r'(?i)(when|where)([\s\S]*?)jack\.?\s?bot': 'I am an eldritch abomination from a land outside of time. Do not ask me such trivial things.',
                    r'(?i)^is\.?\s?this\.?\s?real\.?\s?life\??': 'Yes',
                    r'how([\s\S]*?)base\.?\s?bot': xyzzers,
                    r'^/me spies an? @?jackBot': '/me spies you back',
                    r'^/me has resurrected @jackBot': alive,
                    r'^!basebot$': 'https://github.com/CylonicRaider/basebot',
                    r'^!yaboli$': 'https://github.com/Garmelon/yaboli',
                    r'(?i)^!doclib$': 'https://gitlab.com/DoctorNumberFour/DocLib',
                    r'^!help$': 'I\'m a miscellaneous bot made by @DoctorNumberFour using his Python 3 bot library, DocLib (!doclib for a gitlab link.) I\'m never going to be updated to give info on every jackbox game.',
                    r'(?i)(^|\s|\b)(\d*?|two|three|four|five|six|seven|eight|nine|ten)(^|\s|\b)g(oo|ee)se($|\s|\b)': honk, r'(?i)(^|\s|\b)honk($|\s|\b)': honkkonk,
                    r'^!listusers$': userlist,
                    r'^!copyto &(\w+)$': copyto,
                    r'^!pm$': pm,
                    r'^!loggedin$': loggedin,
                    r'(?i)(^|\s|\b)wordpress($|\s|\b)': kirby})


jackBot.connect()
jackBot.start()
